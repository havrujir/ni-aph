import * as THREE from 'three';
import { Color } from 'three';
import {GameSettings} from './gameSettings'

export class StaticObject extends THREE.Object3D 
{
    // currently used just as floor
    constructor(color: Color, size: number) {
        super();
        this.name = "Static"
        const geometry = new THREE.CircleGeometry(size, 1000);
        const material = new THREE.MeshStandardMaterial( { color: color } );
        this.add(new THREE.Mesh( geometry, material ));
        this.rotation.x = THREE.MathUtils.degToRad(-90);
        this.receiveShadow = true
    }
}