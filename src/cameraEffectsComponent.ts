import { CameraEntity } from "./cameraEntity"
import { PlayerController } from "./playerController"

export class CameraEffectsComponent {
    camera: CameraEntity
    target: PlayerController

    t: number = 0

    constructor(camera: CameraEntity, target: PlayerController){
        this.camera = camera
        this.target = target
    }
    
    onUpdate(delta: number)
    {
        if(this.target.player.animation.isRunning){
            let y = Math.exp(-this.t)*Math.sin(2 * Math.PI * this.t)
            this.t += delta
            this.camera.translateY(y) 
        }
        else
        {
            this.t = 0
        }
    }
}