import { Object3D, Vector3 } from "three";
import { CharacterEntity } from "./characterEntity";
import { GameEventDispatcher } from "./gameEventsDispatcher";

export class CombatResolver
{
    events: GameEventDispatcher
    characters: CharacterEntity[]

    constructor(characters: CharacterEntity[], events: GameEventDispatcher)
    {
        this.characters = characters
        this.events = events
        this.ListenNewCharacter()
    }

    ListenNewCharacter()
    {
        this.events.addEventListener(GameEventDispatcher.newCharacter(), e => 
        {
            this.ListenAttacks(e.message)        
        })
    }

    ListenAttacks(id)
    {
        this.events.addEventListener(GameEventDispatcher.tryAttack(id), e => 
        {
            this.characters.forEach(element => 
            {
                if(element.id == e.message.source.id)
                    return 
                if(this.isInRange(e.message.source, element, e.message.range, e.message.angle))
                {
                    console.log(e.message.source.name + e.message.source.id + " is hitting " + element.name + element.id)
                    element.dispatchEvent(
                    {
                        type: "Action", 
                        message: 
                        {
                            action: "Damaged",
                            damage: e.message.damage
                        }
                    });
                }
            })         
        })
    }

    isInRange(source: Object3D, test: Object3D, range: number, maxAngle = Math.PI)
    {
        let src = new Vector3(0, 0, 1)
        let path = new Vector3(0, 0, 0)
        src.applyQuaternion(source.quaternion)
        path.subVectors(test.position, source.position).normalize()
        let angle = src.angleTo(path)
        return (angle <= maxAngle && source.position.distanceTo(test.position) <= range)
    }
}