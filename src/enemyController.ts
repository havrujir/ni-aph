import { EnemyEntity } from "./enemyEntity";
import { PlayerEntity } from "./playerEntity";

export class EnemyController
{
    enemy: EnemyEntity
    player: PlayerEntity
    prepareAttack: number = 0;
    attacking = false;

    constructor(enemy: EnemyEntity, player: PlayerEntity)
    {
        this.enemy = enemy
        this.player = player
    }

    onUpdate(delta: number)
    {
        if(!this.attacking && this.enemy.position.distanceTo(this.player.position) > 5){
            this.enemy.dispatchEvent(
            {
                type: "Action",
                message:
                { 
                    position: this.player.position,
                    run: false,
                    stop: false,
                    action: "Walk"
                }
            })
        }
        else if(!this.attacking){
            this.enemy.dispatchEvent(
            {
                type: "Action",
                message:
                { 
                    position: this.player.position,
                    run: false,
                    stop: true,
                    action: "Face"
                }
            })
            this.attacking = true;
            this.prepareAttack = 0;
        }
        else if(this.prepareAttack < 1){
            this.prepareAttack += delta
        }
        else{
            this.enemy.dispatchEvent(
            {
                type: "Action",
                message:
                { 
                    
                    run: false,
                    stop: false,
                    action: "Attack"
                }
            })
            this.attacking = false
        }
    }   
}