import {GameEventDispatcher} from "./gameEventsDispatcher"
import { GameSettings } from "./gameSettings"

export class InputManager{
    keys: Map<Object, {down, pressed}>
    mouse: {down: boolean, value: number}
    timeout
    events: GameEventDispatcher

    constructor (events: GameEventDispatcher){
        this.mouse = {down: false, value: 0}
        this.events = events
        this.keys = new Map<Object, {down, pressed}>();
    
        this.addKey(GameSettings.KEY_FW);
        this.addKey(GameSettings.KEY_BACK);
        this.addKey(GameSettings.KEY_LEFT);
        this.addKey(GameSettings.KEY_RIGHT);
        this.addKey(GameSettings.KEY_ROLL);
        this.addKey(GameSettings.KEY_ATTACK);
        this.addKey(GameSettings.KEY_RUN);
    
        window.addEventListener('mousedown', (e) => { this.mouse.down = true;})
        window.addEventListener('mouseup', () => { this.mouse.down = false;})
        window.addEventListener('mouseleave', () => { this.mouse.down = false;})
        window.addEventListener('keydown', (e) => { this.setKey(e.key, true); });
        window.addEventListener('keyup', (e) => { this.setKey(e.key, false); });
        window.addEventListener('mousemove', (e) =>
            {
                if(!this.mouse.down){ 
                    this.mouse.value = 0;
                    return 
                }
                this.mouse.value = e.movementX;
                clearTimeout(this.timeout);
                this.timeout = setTimeout(() => {this.mouse.value = 0;}, 10);
            }
        );
    }

    setKey(name, pressed){
        const state = this.keys[name];
        state.pressed = pressed && !state.down;
        state.down = pressed;
    }

    addKey(name){
        this.keys[name] = { down: false, pressed: false };
    };

    update(delta: number){
        let msg = {mouse: 0, x: 0, z: 0, attack: false, roll: false, run: false, delta: delta}
        let inputMovement = false;
        let inputDirection = false;
        let inputAction = false;

        if(this.keys[GameSettings.KEY_FW].down) {
            msg.z++;
            inputMovement = true;
        }
        if(this.keys[GameSettings.KEY_BACK].down) {
            msg.z--;
            inputMovement = true;
        }
        if(this.keys[GameSettings.KEY_LEFT].down) {
            msg.x--;
            inputMovement = true;
        }
        if(this.keys[GameSettings.KEY_RIGHT].down) {
            msg.x++;
            inputMovement = true;
        }
        if(this.keys[GameSettings.KEY_ATTACK].pressed) {
            msg.attack = true;
            inputAction = true;
        }
        if(this.keys[GameSettings.KEY_ROLL].pressed) {
            msg.roll = true;
            inputAction = true;
        }
        if(this.keys[GameSettings.KEY_RUN].down){
            msg.run = true;
        }
        if(this.mouse.down == true) 
        {
            msg.mouse = this.mouse.value
            inputDirection = true;
        }

        this.keys[GameSettings.KEY_ROLL].pressed = false;
        this.keys[GameSettings.KEY_ATTACK].pressed = false;

        if(inputMovement)
        {
            this.events.dispatchEvent({type: GameEventDispatcher.inputMovement(), message: {x:msg.x, z:msg.z, run:msg.run}});
        }
        if(inputDirection)
        {
            this.events.dispatchEvent({type: GameEventDispatcher.inputCamera(), message: {mouse: msg.mouse}});
        }
        if(inputAction)
        {
            this.events.dispatchEvent({type: GameEventDispatcher.inputActions(), message: {attack: msg.attack, roll: msg.roll}});
        }
    }
}