import { EventDispatcher } from "three";
import { CharacterEntity } from "./characterEntity";
import { GameEventDispatcher } from "./gameEventsDispatcher";

import {Event} from 'three'
import { GameSettings } from "./gameSettings";
import { settings } from "cluster";

export class CharacterActionComponent extends EventDispatcher
{
    events: GameEventDispatcher
    attackTimer: number = 0
    attackRunning: boolean = false
    gcd = 0
    character: CharacterEntity
    attacked: boolean;
    
	constructor(character: CharacterEntity, events: GameEventDispatcher) {
        super()
        this.character = character;
        this.events = events;

        this.addEventListener("Action", e => this.onEvent(e))
    }

    onEvent(e: Event)
    {
        if(e.message.action == "Attack" && !this.attackRunning) {
            this.attackTimer = 0
            this.attackRunning = true
            this.attacked = false
        }
    }

    attackUpdate(delta)
    {
        if(this.attackRunning)
        {
            if(this.attackTimer < 0.50){}
            else if (this.attackTimer > GameSettings.ACTION_DURATION){
                this.attackRunning = false
            }
            else if(!this.attacked)
            {
                console.log(this.character.id + " is attacking")
                this.events.dispatchEvent(
                {
                    type: GameEventDispatcher.tryAttack(this.character.id), 
                    message: 
                    {
                        source: this.character,
                        damage: GameSettings.ENEMY_DAMAGE, 
                        range: 5, 
                        angle: Math.PI/4
                    }
                });
                this.attacked = true
            }
            this.attackTimer += delta
        }
    }

    onUpdate(delta)
    {
        this.attackUpdate(delta)
    }
}