import { Mesh, Vector3 } from 'three';
import { CharacterEntity } from './characterEntity';
import { GameEventDispatcher } from './gameEventsDispatcher';
import { GameSettings } from './gameSettings';

export  class CollisionResolver {
    events: GameEventDispatcher
    characters: CharacterEntity[]
    constructor(characters: CharacterEntity[], events: GameEventDispatcher)
    {
        this.characters = characters
        this.events = events
    }

    IsIntersecting(a: Mesh, b: Mesh)
    {
        a.updateMatrixWorld()
        b.updateMatrixWorld()
        let aTest = a.geometry.boundingSphere.clone()
        let bTest = b.geometry.boundingSphere.clone()
        aTest.applyMatrix4(a.matrixWorld)
        bTest.applyMatrix4(b.matrixWorld)
        return aTest.intersectsSphere(bTest)
    }

    IsOutOfBounds(a: Vector3)
    {
        return a.distanceTo(new Vector3(0,0,0)) >= GameSettings.ARENA_RADIUS - 0.5
    }

    CanMove(mesh: Mesh)
    {
        if(this.IsOutOfBounds(mesh.position))
            return false
        else
        {
            let b = true;
            this.characters.forEach(element => 
            {
                let x = this.IsIntersecting(mesh, element.movement)
                if(x && element.movement.id != mesh.id)
                    b = false;
            })
            return b
        }
    }
}
