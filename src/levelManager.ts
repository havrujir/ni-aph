import { GLTF } from 'three/examples/jsm/loaders/GLTFLoader';
import { SkeletonUtils } from 'three/examples/jsm/utils/SkeletonUtils'
import { CameraEntity } from './cameraEntity';
import { CombatResolver } from './combatResolver'
import { CollisionResolver } from './collisionResolver';
import { PlayerEntity } from './playerEntity';
import { InputManager } from './inputManager';
import { DirectionalLight, Scene, Vector3 } from 'three';
import { GameEventDispatcher } from './gameEventsDispatcher';
import { CharacterEntity } from './characterEntity';
import { EnemyEntity } from './enemyEntity';
import { Level } from './levelGenerator';
import { GameSettings } from './gameSettings';
import { EnemyController } from './enemyController';
import { PlayerController } from './playerController';

export class LevelManager
{
    player: PlayerEntity
    gltf: GLTF
    camera: CameraEntity
    events: GameEventDispatcher
    input: InputManager
    combat: CombatResolver
    collision: CollisionResolver
    level: Level

    characters: CharacterEntity[]
    controls: (EnemyController | PlayerController)[]

    constructor(gltf: GLTF) 
    {
        this.gltf = gltf
        this.StartLevel()
    }

    // Listen(LevelFinished)

    StartLevel()
    {   
        this.events = new GameEventDispatcher()
        this.characters = new Array;
        this.controls = new Array;
        this.combat = new CombatResolver(this.characters, this.events)
        this.collision = new CollisionResolver(this.characters, this.events)
        this.input = new InputManager(this.events) 
        let obj
        obj = SkeletonUtils.clone(this.gltf.scene)
        this.player = new PlayerEntity(obj, this.gltf.animations, this.collision, this.events)
        this.level = new Level(this.events, this.characters, this.player, this.collision)

        let light = this.makeLight()
        this.level.add(light)
        
        this.level.add(this.player)
        this.player.traverse(child => 
        {
            child.castShadow = true
        })
        this.characters.push(this.player)

        let control = new PlayerController(this.player, this.events)
        this.controls.push(control)
        this.camera = new CameraEntity(GameSettings.CAMERA_FOV, GameSettings.CAMERA_ASPECT, GameSettings.CAMERA_NEAR, GameSettings.CAMERA_LOD,this.events, control)
        //this.player.add(this.camera)
        this.events.dispatchEvent(
        {
            type: GameEventDispatcher.newCharacter(), 
            message: this.player.id
        })

        this.ListenDeaths()
        this.ListenAddEnemy()
    }

    // Fixme - if player, restart game
    ListenDeaths()
    {
        this.events.addEventListener(GameEventDispatcher.isDead(), e => 
        {
            if(this.player.id == e.message)
            {
                this.StartLevel()
            }
            for(let i = 0; i < this.characters.length; i++)
            {
                if(this.characters[i].id == e.message)
                {
                    this.level.remove(this.characters[i])
                    this.characters.splice(i, 1)
                    break
                }
            }
        })
    }

    ListenAddEnemy()
    {
        this.events.addEventListener(GameEventDispatcher.addEnemy(), e => 
        {
            let obj;
            obj = SkeletonUtils.clone(this.gltf.scene);

            // add new enemy
            let enemy = new EnemyEntity(obj, this.gltf.animations, this.collision, this.events);
            let control = new EnemyController(enemy, this.player)
            enemy.traverse(child => {child.castShadow = true})
            this.characters.push(enemy);
            this.controls.push(control)
            enemy.position.copy(e.message.position)
            this.events.dispatchEvent({type: GameEventDispatcher.newCharacter(), message: enemy.id})


            this.ListenDeaths()
            this.level.add(enemy)
        })
    }

    update(delta: number)
    {
        // get new input actions
        this.input.update(delta);

        this.level.update(delta);

        // update all controlls
        this.controls.forEach(element =>
        {
            element.onUpdate(delta)
        })
        // update all characters
        this.characters.forEach(element => 
        {
            element.onUpdate(delta);
        })
        this.camera.onUpdate(delta);
    }

    makeLight()
    {
        let color = 0xFFFFFF
        let intensity = 1
        let light = new DirectionalLight(color, intensity)

        light.position.set(20,50,-40)

        light.shadow.mapSize.height = 1024
        light.shadow.mapSize.width = 1024
        light.castShadow = true

        light.shadow.camera.left = -100
        light.shadow.camera.right = 100
        light.shadow.camera.top = 100
        light.shadow.camera.bottom = -100

        return light   
    }
}