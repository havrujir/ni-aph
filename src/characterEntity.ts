import { Event, AnimationClip, Object3D } from 'three';
import { GameEventDispatcher } from './gameEventsDispatcher';
import { CharacterAnimationComponent } from './characterAnimationComponent';
import { CharacterMovementComponent } from './characterMovementComponent';
import { CharacterActionComponent } from './characterActionComponent';
import { CollisionResolver } from './collisionResolver';
import { CharacterCombatComponent } from './characterCombatComponent';

export class CharacterEntity extends Object3D 
{
    action: CharacterActionComponent
    movement: CharacterMovementComponent
    animation: CharacterAnimationComponent
    combat: CharacterCombatComponent

    x: number
    z: number

    components = []

    constructor(animations: AnimationClip[], model: Object3D, collision: CollisionResolver, events: GameEventDispatcher) 
    {
        super()
        super.add(model)
        
        this.animation = new CharacterAnimationComponent(this, animations)
        this.movement = new CharacterMovementComponent(this, collision)
        this.action = new CharacterActionComponent(this, events)
        this.combat = new CharacterCombatComponent(this, events)

        this.components.push(this.action)
        this.components.push(this.movement)
        this.components.push(this.animation)
        this.components.push(this.combat)

        this.addEventListener("Action", e => this.onEvent(e))
    }

    onEvent(e: Event)
    {
        if(e.type == "Action") 
        {
            this.components.forEach(element => {
                element.dispatchEvent(
                {
                    type: "Action",
                    message: 
                    {
                        position: e.message.position,
                        run: e.message.run,
                        action: e.message.action,
                        stop: e.message.stop,
                        damage: e.message.damage,
                    }
                })    
            });
        }
    }

    onUpdate(delta: number) 
    {  
        this.components.forEach(element => {
            element.onUpdate(delta)
        });
    }
}