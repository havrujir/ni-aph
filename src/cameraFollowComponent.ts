import { Vector3 } from "three"
import { CameraEntity } from "./cameraEntity"
import { PlayerController } from "./playerController"

export class CameraFollowComponent {
    camera: CameraEntity
    target: PlayerController

    
    OffsetZ: Vector3 = new Vector3(0,0,15)
    OffsetY: Vector3 = new Vector3(0,10,0)

    constructor(camera: CameraEntity, target: PlayerController){
        this.camera = camera
        this.target = target
        this.camera.translateOnAxis(this.OffsetZ, -1)
        this.camera.position.setY(this.OffsetY.y)
    }
    
    onUpdate(delta: number)
    {
        this.camera.position.copy(this.target.player.position)
        this.camera.rotation.copy(this.target.face)
        this.camera.translateOnAxis(this.OffsetZ, -1)
        this.camera.position.setY(this.OffsetY.y)
        let lookAt = this.target.player.position.clone()
        lookAt.y += 4
        this.camera.lookAt(lookAt)
    }
}