import { Euler, Object3D, Quaternion, Vector3 } from "three";
import { CameraEntity } from "./cameraEntity"
import { PlayerController } from "./playerController"

export class CameraOffsetComponent {
    camera: CameraEntity
    target: PlayerController

    previous = new Vector3
    scalar = new Vector3

    translateByX = 0
    translateByZ = 0
    translateByXLimit = 6
    translateByZLimit = 3

    constructor(camera: CameraEntity, target: PlayerController){
        this.camera = camera
        this.target = target
    }

    onUpdate(delta: number)
    {
        let cur = this.target.player.position
        let pre = this.previous
        let speed = delta * 1

        if(!cur.equals(pre)){
            if(this.target.moves.run)
                speed *= 1.5

            if(this.target.moves.x == -1)
            {
                if(this.translateByX > -this.translateByXLimit)
                    this.translateByX += -1 * delta * 6
            }
            else if(this.target.moves.x == 1){
                if(this.translateByX < this.translateByXLimit)
                    this.translateByX += 1 * delta * 6
                
            }
            else{
                if(this.translateByX > 0){
                    this.translateByX -= delta * 6
                    if(this.translateByX < 0){
                        this.translateByX = 0
                    }
                }

                if(this.translateByX < 0){
                    this.translateByX += delta * 6
                    if(this.translateByX > 0){
                        this.translateByX = 0
                    }
                }
            }
            if(this.target.moves.run){
                if(this.target.moves.z == -1)
                {
                    if(this.translateByZ > -this.translateByZLimit)
                        this.translateByZ += -1 * delta * 6
                }
                else if(this.target.moves.z == 1){
                    if(this.translateByZ < this.translateByZLimit)
                        this.translateByZ += 1 * delta * 6
                    
                }
            }
            else{
                if(this.translateByZ > 0){
                    this.translateByZ -= delta * 6
                    if(this.translateByZ < 0){
                        this.translateByZ = 0
                    }
                }

                if(this.translateByZ < 0){
                    this.translateByZ += delta * 6
                    if(this.translateByZ > 0){
                        this.translateByZ = 0
                    }
                }
            }
            
        }
        else{
            if(this.translateByX > 0){
                this.translateByX -= delta * 6
                if(this.translateByX < 0){
                    this.translateByX = 0
                }
            }

            if(this.translateByX < 0){
                this.translateByX += delta * 6
                if(this.translateByX > 0){
                    this.translateByX = 0
                }
            }

            if(this.translateByZ > 0){
                this.translateByZ -= delta * 6
                if(this.translateByZ < 0){
                    this.translateByZ = 0
                }
            }

            if(this.translateByZ < 0){
                this.translateByZ += delta * 6
                if(this.translateByZ > 0){
                    this.translateByZ = 0
                }
            }
        }
        this.camera.translateX(-this.translateByX)
        this.camera.translateZ(this.translateByZ)

        this.previous.copy(this.target.player.position)
    }
}
