import * as THREE from 'three';
import {GameEventDispatcher} from "./gameEventsDispatcher"
import {CharacterEntity} from './characterEntity';
import { CollisionResolver } from './collisionResolver';

export class PlayerEntity extends CharacterEntity 
{
    
    constructor(model: THREE.Object3D, animations: THREE.AnimationClip[], collision: CollisionResolver, events: GameEventDispatcher) 
    {
        super(animations, model, collision, events)
        this.name = "Player"
    }

    onUpdate(delta: number) 
    {
        super.onUpdate(delta)
    }
}