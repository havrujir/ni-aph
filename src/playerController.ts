import * as THREE from 'three';
import {GameEventDispatcher} from "./gameEventsDispatcher"
import {GameSettings} from "./gameSettings"
import { Object3D } from 'three';
import { PlayerEntity } from './playerEntity';

export class PlayerController extends Object3D
{
    moves: {x: number, z: number, run: boolean}
    actions: {attack: boolean, roll: boolean}
    
    isFreshMovement = false;
    isFreshAction = false;
    
    currentSpeed = 0;
    
    face: THREE.Euler
    nextFace = 0;

    events: GameEventDispatcher
    player: PlayerEntity

    constructor(player: PlayerEntity, events: GameEventDispatcher) 
    {
        super()
        this.events = events
        this.player = player
        
        this.moves = {x: 0, z: 0, run: false}
        this.actions = {attack: false, roll: false}
        this.face = this.player.rotation.clone()

        events.addEventListener(GameEventDispatcher.inputMovement(), e => 
        {
            this.moves = e.message; 
            this.isFreshMovement = true
        })
        events.addEventListener(GameEventDispatcher.inputActions(), e => 
        {
            this.actions = e.message; 
            this.isFreshAction = true
        })
        events.addEventListener(GameEventDispatcher.inputCamera(), e => 
        {
            this.nextFace = e.message.mouse
        })
    }

    rotateFace(speed: number)
    {
        this.face.y += (-speed * this.nextFace)
        this.nextFace = 0
        this.events.dispatchEvent(
        {
            type: GameEventDispatcher.playerPositionChanged(), 
            message: 
            {
                pos: this.player.position, 
                rot: this.face
            }
        })
    }

    moveRelativeToFace(speed: number, delta: number)
    {

        if(this.moves.z == -1 && this.moves.x == 0)
            this.rotation.y = this.face.y + Math.PI
        else
        {
            this.rotation.y = this.face.y + (Math.PI / 2) * -this.moves.x;
            this.rotation.y += (Math.PI / 4) * this.moves.z * this.moves.x;
        }

        let directionSpot = new Object3D
        directionSpot.rotation.copy(this.rotation)
        directionSpot.position.copy(this.player.position)
        directionSpot.translateZ(1)

        let action;
        if(this.actions.roll && this.isFreshAction)
        {
            action = "Roll";
        }
        if(this.actions.attack && this.isFreshAction)
        {
            action = "Attack";
        }
        this.player.dispatchEvent(
        {
            type: "Action",
            message:
            { 
                position: directionSpot.position,
                run: this.moves.run,
                stop: !this.isFreshMovement,
                action: action,
                x: this.moves.x,
                z: this.moves.z
            }
        })
    }

    onUpdate(delta: number) 
    {
        this.rotateFace(delta * GameSettings.MOUSE_SPEED)

        if(this.isFreshMovement)
        {
            this.currentSpeed = this.moves.run ? GameSettings.PLAYER_RUN : GameSettings.PLAYER_WALK
            this.moveRelativeToFace(this.currentSpeed, delta)
        }
        else 
        {
            this.currentSpeed = 0
            this.moveRelativeToFace(this.currentSpeed, delta)
        }
        this.isFreshMovement = false;
        this.isFreshAction = false;
    }
}