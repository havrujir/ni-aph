import { Sprite } from "three";
import { CharacterEntity } from "./characterEntity";
import { GameEventDispatcher } from "./gameEventsDispatcher";

import {Event} from 'three'

export class CharacterCombatComponent extends Sprite
{
    events: GameEventDispatcher
    character: CharacterEntity

    
    healthMax: number = 100
    health = this.healthMax
    damage: number = 20
    
	constructor(character: CharacterEntity, events: GameEventDispatcher) {
        super()
        this.character = character;
        this.events = events;

        this.setBar()
        this.character.add(this)
        this.addEventListener("Action", e => this.onEvent(e))
    }

    onEvent(e: Event)
    {
        if(e.message.action == "Damaged") {
            this.health -= e.message.damage

            this.scale.set(3*(this.health/this.healthMax),0.1,0.1)
            if(this.health <= 0) {
                this.events.dispatchEvent({type: GameEventDispatcher.isDead(), message: this.character.id})
            }
        }
    }

    onUpdate(delta: number)
    {
    }

    setBar()
    {
        this.position.y += 6
        this.scale.x = 3
        this.scale.y = 0.1;
        this.material.color.set(0x330000)
    }
}