import { Event, AnimationClip, AnimationMixer, LoopOnce } from "three";
import { CharacterEntity } from "./characterEntity";
import { GameSettings } from "./gameSettings";

export class CharacterAnimationComponent extends AnimationMixer
{   
    animations: AnimationClip[];
    gcd = 0;
    isRunning = false;

    constructor(character: CharacterEntity, animations: AnimationClip[]) 
    {
        super(character)
        this.animations = animations;
        this.clipAction(this.animations[3]).play()
        this.clipAction(this.animations[0]).play()
        this.addEventListener("Action", (e) => this.onEvent(e))
    }

    onEvent(e: Event) 
    {
        if(this.isRunning == false) 
        {
            if(e.message.stop)
            {
                this.clipAction(this.animations[0]).weight = 1
                this.clipAction(this.animations[3]).weight = 0    
            }
            else
            {
                this.clipAction(this.animations[0]).weight = 0
                this.clipAction(this.animations[3]).weight = 1      
            }

            if(e.message.action == "Roll")
            {
                this.isRunning = true
                this.gcd = 0
                this.clipAction(this.animations[2]).stop()
                this.clipAction(this.animations[0]).weight = 0
                this.clipAction(this.animations[3]).weight = 0
                this.clipAction(this.animations[2]).setLoop(LoopOnce, 1)
                this.clipAction(this.animations[2]).play()
            }
            else if(e.message.action == "Attack")
            {
                this.isRunning = true
                this.gcd = 0
                this.clipAction(this.animations[4]).stop()
                this.clipAction(this.animations[0]).weight = 0
                this.clipAction(this.animations[3]).weight = 0
                this.clipAction(this.animations[4]).setLoop(LoopOnce, 1)
                this.clipAction(this.animations[4]).play()
            }
            this.clipAction(this.animations[3]).timeScale = e.message.run ? 1.5 : 1
        }
    }

    onUpdate(delta: number)
    {
        this.gcd += delta
        if(this.gcd > GameSettings.ACTION_DURATION){
            this.isRunning = false
        }
        this.update(delta)
    }
}