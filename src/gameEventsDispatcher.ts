import { EventDispatcher } from 'three'

export class GameEventDispatcher extends EventDispatcher 
{
    static inputMovement() { return "InputMovement" }
    static inputActions() { return "InputActions" }
    static inputCamera() { return "InputCamera" }
    static cameraDirection() { return "CameraDirection" }
    static playerPositionChanged() { return "PlayerPositionChanged" }
    static playerRotation() { return "PlayerRotation" }
    static tryAttack(id: number) { return "TryToAttack" + id }
    static tryMove(id: number) { return "TryMoveAt" + id }
    static canMoveResponse(id: number) { return "CanMoveResponse" + id }
    static takeDamage(id: number) { return "TakeDamage" + id }
    static isDead() { return "IsDead" }
    static newCharacter() { return "NewCharacter" }
    static addEnemy() { return "AddEnemy" }
}