import { CharacterEntity } from './characterEntity'
import { AnimationClip, Object3D, Vector3 } from 'three';
import { GameEventDispatcher } from './gameEventsDispatcher';
import { CollisionResolver } from './collisionResolver';

export class EnemyEntity extends CharacterEntity
{
    constructor(model: Object3D, animations: AnimationClip[], collision: CollisionResolver, events: GameEventDispatcher) 
    {
        super(animations, model, collision, events)
        this.name = "Enemy"
    }

    onUpdate(delta: number) 
    {
        super.onUpdate(delta)
    }
}