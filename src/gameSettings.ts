export class GameSettings 
{
    static PLAYER_WALK = 10
    static PLAYER_RUN = 20
    static PLAYER_DAMAGE = 20
    static PLAYER_HEALTH = 1000

    static ENEMY_WALK = 10
    static ENEMY_RUN = 20

    static MOUSE_SPEED = 1.25

    static KEY_RUN = 'c'
    static KEY_ATTACK = 'q'
    static KEY_ROLL = 'e'
    static KEY_FW = 'w'
    static KEY_BACK = 's'
    static KEY_LEFT = 'a'
    static KEY_RIGHT = 'd'

    static ENEMY_HEALTH = 100
    static ENEMY_DAMAGE = 20

    static CAMERA_FOV = 75
    static CAMERA_ASPECT = 16/9
    static CAMERA_NEAR = 0.1
    static CAMERA_LOD = 1000

    static ARENA_RADIUS = 50
    static ACTION_DURATION = 1
}