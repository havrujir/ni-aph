import { Color, MathUtils, Mesh, MeshNormalMaterial, Scene, SphereGeometry } from 'three';
import { GameEventDispatcher } from './gameEventsDispatcher';
import { CharacterEntity } from './characterEntity';
import { StaticObject } from './staticEntity';
import { GameSettings } from './gameSettings';
import { PlayerEntity } from './playerEntity';
import { CollisionResolver } from './collisionResolver';

export class Level extends Scene
{
    events: GameEventDispatcher
    spawn: number = 5 // FIXME: temporal enemy spawn timer

    floor: StaticObject
    characters: CharacterEntity[] // all characters interacting with each other
    player: PlayerEntity
    collision: CollisionResolver

    prepareAttack = 0

    constructor(events: GameEventDispatcher, characters: CharacterEntity[], player: PlayerEntity, collision: CollisionResolver)
    {
        super()
        this.events = events
        this.player = player
        this.characters = characters
        this.collision = collision
        let colornum = (Math.random() * 0xffffff)
        let color = new Color(colornum)
        this.background = color

        GameSettings.ARENA_RADIUS = MathUtils.randFloat(20, 80)
        const floor = new StaticObject(new Color(colornum), GameSettings.ARENA_RADIUS)
        floor.traverse(element => 
        {
            element.receiveShadow = true
        })
        this.add(floor)
    }

    update(delta: number)
    {
        this.spawn += delta;
        if(this.spawn > 10) {
            this.spawn = 0;
        }
        if(this.characters.length <= 3 && this.spawn == 0){
            
            let mesh = new Mesh(new SphereGeometry(1), new MeshNormalMaterial())
            mesh.geometry.computeBoundingSphere()
            mesh.position.x = MathUtils.randFloat(-GameSettings.ARENA_RADIUS, GameSettings.ARENA_RADIUS)
            mesh.position.z = MathUtils.randFloat(-GameSettings.ARENA_RADIUS, GameSettings.ARENA_RADIUS)
            while(!this.collision.CanMove(mesh)){
                mesh.position.x = MathUtils.randFloat(-GameSettings.ARENA_RADIUS, GameSettings.ARENA_RADIUS)
                mesh.position.z = MathUtils.randFloat(-GameSettings.ARENA_RADIUS, GameSettings.ARENA_RADIUS)
            }
            this.events.dispatchEvent(
            {
                type: GameEventDispatcher.addEnemy(),
                message: {position: mesh.position}
            })
        }
    }
}