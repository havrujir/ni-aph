import { Mesh, MeshNormalMaterial, SphereGeometry, Event, Vector3 } from "three";
import { CharacterEntity } from "./characterEntity";
import { CollisionResolver } from "./collisionResolver";
import { GameSettings } from "./gameSettings";

export class CharacterMovementComponent extends Mesh
{
    character: CharacterEntity
    collision: CollisionResolver
    target = new Vector3(0, 0, 0)
    speed = 0
    gcd = 20
    isRunning = false

    constructor(character: CharacterEntity, collision: CollisionResolver)
    {
        super(new SphereGeometry(1), new MeshNormalMaterial())
        this.character = character;
        this.position.copy(character.position)
        this.collision = collision;
        this.geometry.computeBoundingSphere()
        this.addEventListener("Action", (e) => this.onEvent(e))
    }

    onEvent(e: Event)
    {
        
        if((e.message.action == "Attack" || e.message.action == "Roll") && this.gcd > GameSettings.ACTION_DURATION)
        {
            this.gcd = 0
            this.isRunning = true
        }
        if(e.message.action == "Face"){
            this.target.copy(e.message.position)
            this.speed = 0
        }
        if(e.message.position != undefined && !this.isRunning) {
            this.target.copy(e.message.position)
            this.speed = e.message.stop ? 0 : e.message.run ? GameSettings.PLAYER_RUN : GameSettings.PLAYER_WALK
        }
        if(this.character.x != undefined){
            this.character.x = e.message.x
        }
    }

    onUpdate(delta: number)
    {
        
        if(this.isRunning){
            if(this.gcd > GameSettings.ACTION_DURATION)
                this.isRunning = false
            else {
                this.gcd += delta
            }
        }
        else{
            this.character.lookAt(this.target)
            this.lookAt(this.target)
        }
    
        if(this.speed == 0){
            this.character.lookAt(this.target)
            this.lookAt(this.target)
            return
        }


        this.translateZ(delta * this.speed)
        if(this.collision.CanMove(this))
        {
            this.character.translateZ(delta * this.speed)
        }
        this.position.copy(this.character.position)
    }
}