import { GLTF, GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader.js'
import { OBJLoader } from 'three/examples/jsm/loaders/OBJLoader.js'
import { BasicShadowMap, Clock, LoadingManager, Object3D, WebGLRenderer } from 'three';
import { LevelManager } from './levelManager';

let renderer: WebGLRenderer
let clock: Clock
let logic: LevelManager
let model: GLTF
let weapon: Object3D

function start()
{
	loader()
}

function loader()
{
	const manager = new LoadingManager()
	manager.onLoad = loaderSword
	const gltfLoader = new GLTFLoader(manager)	
	gltfLoader.load( 'assets/Character.glb', function (e) 
	{
		model = e
	})
}

function loaderSword(){
	const manager = new LoadingManager()
	manager.onLoad = init
	const objLoader = new OBJLoader(manager)	
	objLoader.load( 'assets/Sword.obj', function (e) 
	{
		weapon = e
		model.scene.traverse(element => 
		{
			if(element.name == "PalmR")
			{
				weapon.rotateZ(Math.PI/2)
				element.add(weapon.clone())
			}
		})
	})
}

function init()
{
	let canvas = document.getElementById('gameCanvas') as HTMLCanvasElement
	
	renderer = new WebGLRenderer(
	{
		canvas: canvas, 
		antialias: true 
	})
	document.body.appendChild(renderer.domElement)

	renderer.setSize(canvas.width, canvas.height)

	renderer.shadowMap.enabled = true
	renderer.shadowMap.type = BasicShadowMap

	logic = new LevelManager(model);

	renderer.render(logic.level, logic.camera)

	clock = new Clock()
	clock.autoStart = true

	loop()
}

function loop() 
{
	let delta = clock.getDelta()
	logic.update(delta)
	renderer.render( logic.level, logic.camera )
	requestAnimationFrame(loop)
}

export default start()
