import { PerspectiveCamera } from 'three';
import { CameraEffectsComponent } from './cameraEffectsComponent';
import { CameraFollowComponent } from './cameraFollowComponent';
import { CameraOffsetComponent } from './cameraOffsetComponent';
import { GameEventDispatcher } from './gameEventsDispatcher'
import { PlayerController } from './playerController';

export class CameraEntity extends PerspectiveCamera{
    
    follow: CameraFollowComponent
    components = []
    offset: CameraOffsetComponent
    effects: CameraEffectsComponent
    
    constructor(fov, aspect, near, lod, events: GameEventDispatcher, target: PlayerController) 
    {
        super(fov, aspect, near, lod)
        this.name = "Camera"

        this.follow = new CameraFollowComponent(this, target)
        this.offset = new CameraOffsetComponent(this, target)
        this.effects = new CameraEffectsComponent(this, target)

        this.components.push(this.follow)
        this.components.push(this.offset)
        this.components.push(this.effects)
    }

    onUpdate(delta: number)
    {
        this.components.forEach(element =>
        {
            element.onUpdate(delta)
        })
    }
}
