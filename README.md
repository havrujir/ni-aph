# Semestrální projekt
### NI-APH
Tento projekt byl vyvíjen za pomoci knihovny Three.Js. Cílem bylo vytvořit arénu, ve které se hráč potýká s vlnami nepřátel. V tuto chvíli bojuje hráč v kruhové aréně náhodné
velikosti s nepřáteli. Úroveň končí ve chvíli, kdy hráč zemře a spustí se další.

## Funkce
Hra je v tuto chvíli plně hratelná, úrovně jsou generovány náhodně a spouští se hned po startu, případně po smrti hráče.
Hra v tuto chvíli obsahuje:

> Základní kolizní model, kdy postavy skrz sebe neprocházejí a nespawnují se v sobě

> Mechaniku útoku směrem vpřed v daném úhlu a vzdálenosti

> Umělou inteligenci, která pouze běží k hráči a při přiblížení zahájí přípravu a následný útok, aby bylo hráči umožněno reagovat a vyhnout se

> Hráče, co umí sprintovat, útočit a rollovat. Rollování zatím funguje jen k zafixování směru a rychlosti pohybu

> Postavy, které jsou částečně animované, ovšem například chybí animace pro útok z místa a nejsou korektně míchané

> Akční kameru z pohledu 3. osoby, která oddaluje resp. přibližuje pokud hráč sprintuje směrem dopředu/dozadu, reaguje na pohyb do stran a otřese se při provedení akce

## Jak hrát

### Ovládání
> pohyb: W, S, A, D + tažení myší

> sprint: C

> rollování: E

> útok: Q

Úroveň je nastavená tak, že je nemožné přežít dlouho na jednom místě. Klíčem je tedy zůstat v pohybu a přibližovat se k nepříteli pouze za účelem útoku. Je možné použít kombinaci
sprintu a rollu k zafixování pohybu a přípravě na další útok. Rádius útoku není velký. Nepřátelé můžou útočit na sebe navzájem (původně chyba, nyní funkce) a je tedy možné
procházet úrovní správným směřováním nepřátelských útoků.

## Architektura hry
![architecture.png](https://gitlab.fit.cvut.cz/havrujir/ni-aph/raw/master/architecture.png)
Zde je u každého objektu uveden jeho rodič. Šipky znázorňují posílané zprávy a přímá čára pak důležité reference. Základem hry je třída "LevelManager", která inicializuje
nový level a navazuje na hlavní herní smyčku, tedy volá funkce "update" stěžejních objektů na scéně.

## Instalace a spuštění
> Instalace probíhá příkazem "npm install"

> Spouštění aplikace v tuto chvíli probíhá příkazem "npm run dev"

## Reference
Aplikace používá knihovny z balíčku "three/examples" pro načítání 3D modelů a klonování "SkinnedMesh"
Autorem modelu postav je Quaternius, který tento model zveřejnil pod licencí "CC0 1.0 Univerzální (CC0 1.0)"
https://www.patreon.com/quaternius